--------------------------------------------------------
--  File created - Thursday-October-10-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table PRODUCT_TABLE
--------------------------------------------------------

  CREATE TABLE "MER_DB"."PRODUCT_TABLE" 
   (	"PRODUCT_NO" VARCHAR2(20 BYTE), 
            -- "PACKING_TYPE" NUMBER(6,3), 
            -- "UNIT_PRICE" NUMBER(15,2), 
            -- "USE_DATE" VARCHAR2(10 BYTE), 
            -- "USE_FLAG" VARCHAR2(1 BYTE), 
	"PRODUCT_CNAME" VARCHAR2(50 BYTE), 
	"PRODUCT_VNAME" VARCHAR2(80 BYTE), 
	"PRODUCT_ENAME" VARCHAR2(50 BYTE), 
	"PRODUCT_KIND" VARCHAR2(1 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index PRODUCT_PRICE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MER_DB"."PRODUCT_PRICE_PK" ON "MER_DB"."PRODUCT_TABLE" ("PRODUCT_NO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table PRODUCT_TABLE
--------------------------------------------------------

  ALTER TABLE "MER_DB"."PRODUCT_TABLE" MODIFY ("PRODUCT_NO" NOT NULL ENABLE);
  ALTER TABLE "MER_DB"."PRODUCT_TABLE" MODIFY ("PACKING_TYPE" NOT NULL ENABLE);
  ALTER TABLE "MER_DB"."PRODUCT_TABLE" MODIFY ("USE_DATE" NOT NULL ENABLE);
  ALTER TABLE "MER_DB"."PRODUCT_TABLE" MODIFY ("USE_FLAG" NOT NULL ENABLE);
  ALTER TABLE "MER_DB"."PRODUCT_TABLE" ADD CONSTRAINT "PRODUCT_PRICE_PK" PRIMARY KEY ("PRODUCT_NO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;

--------------------------------------------------------
-- Nov 20

ALTER TABLE PRODUCT_TABLE ADD BRAND_NO VARCHAR2(1);
ALTER TABLE PRODUCT_TABLE ADD PRODUCT_VNAME2 NVARCHAR2(240);

--------------------------------------------------------
-- Dec 09

ALTER TABLE PRODUCT_TABLE ADD P_1 VARCHAR2(5);
ALTER TABLE PRODUCT_TABLE ADD P_2 VARCHAR2(5);
ALTER TABLE PRODUCT_TABLE ADD P_3 VARCHAR2(6);
ALTER TABLE PRODUCT_TABLE ADD P_4 VARCHAR2(4);
