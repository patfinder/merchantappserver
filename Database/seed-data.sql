

-- product_table: PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG, PRODUCT_CNAME, PRODUCT_VNAME2, PRODUCT_ENAME, PRODUCT_KIND, P_1
-- insert into product_table(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG, PRODUCT_CNAME, PRODUCT_VNAME2, PRODUCT_ENAME, PRODUCT_KIND, P_1) values('V801', 25, 230000, '2019.08.01','1','V801','V801','V801','1', '04');
-- insert into product_table(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG, PRODUCT_CNAME, PRODUCT_VNAME2, PRODUCT_ENAME, PRODUCT_KIND, P_1) values('V802', 25, 230000, '2019.08.01','1','V802','V802','V802','2', '04');
-- insert into product_table(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG, PRODUCT_CNAME, PRODUCT_VNAME2, PRODUCT_ENAME, PRODUCT_KIND, P_1) values('V803', 25, 230000, '2019.08.01','1','V803','V803','V803','3', '04');
-- insert into product_table(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG, PRODUCT_CNAME, PRODUCT_VNAME2, PRODUCT_ENAME, PRODUCT_KIND, P_1) values('V804', 25, 230000, '2019.08.01','1','V804','V804','V804','5', '04');
-- insert into product_table(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG, PRODUCT_CNAME, PRODUCT_VNAME2, PRODUCT_ENAME, PRODUCT_KIND, P_1) values('V805', 25, 230000, '2019.08.01','1','V805','V805','V805','R', '04');
-- insert into product_table(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG, PRODUCT_CNAME, PRODUCT_VNAME2, PRODUCT_ENAME, PRODUCT_KIND, P_1) values('V806', 25, 230000, '2019.08.01','1','V806','V806','V806','1', '06');
-- insert into product_table(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG, PRODUCT_CNAME, PRODUCT_VNAME2, PRODUCT_ENAME, PRODUCT_KIND, P_1) values('V807', 25, 230000, '2019.08.01','1','V807','V807','V807','1', '06');
-- insert into product_table(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG, PRODUCT_CNAME, PRODUCT_VNAME2, PRODUCT_ENAME, PRODUCT_KIND, P_1) values('V808', 25, 230000, '2019.08.01','1','V808','V808','V808','1', '06');
-- insert into product_table(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG, PRODUCT_CNAME, PRODUCT_VNAME2, PRODUCT_ENAME, PRODUCT_KIND, P_1) values('V809', 25, 230000, '2019.08.01','1','V809','V809','V809','1', '06');


insert into product_table(PRODUCT_NO, PRODUCT_CNAME, PRODUCT_VNAME, PRODUCT_ENAME, PRODUCT_KIND, BRAND_NO, PRODUCT_VNAME2, P_1) values('V801', 'C801', 'V801', 'E801', 'P', '1', 'V801', '04');
insert into product_table(PRODUCT_NO, PRODUCT_CNAME, PRODUCT_VNAME, PRODUCT_ENAME, PRODUCT_KIND, BRAND_NO, PRODUCT_VNAME2, P_1) values('V802', 'C802', 'V802', 'E802', 'P', '2', 'V802', '04');
insert into product_table(PRODUCT_NO, PRODUCT_CNAME, PRODUCT_VNAME, PRODUCT_ENAME, PRODUCT_KIND, BRAND_NO, PRODUCT_VNAME2, P_1) values('V803', 'C803', 'V803', 'E803', 'P', '3', 'V803', '04');
insert into product_table(PRODUCT_NO, PRODUCT_CNAME, PRODUCT_VNAME, PRODUCT_ENAME, PRODUCT_KIND, BRAND_NO, PRODUCT_VNAME2, P_1) values('V804', 'C804', 'V804', 'E804', 'P', '5', 'V804', '04');
insert into product_table(PRODUCT_NO, PRODUCT_CNAME, PRODUCT_VNAME, PRODUCT_ENAME, PRODUCT_KIND, BRAND_NO, PRODUCT_VNAME2, P_1) values('V805', 'C805', 'V805', 'E805', 'P', 'R', 'V805', '04');
insert into product_table(PRODUCT_NO, PRODUCT_CNAME, PRODUCT_VNAME, PRODUCT_ENAME, PRODUCT_KIND, BRAND_NO, PRODUCT_VNAME2, P_1) values('V806', 'C806', 'V806', 'E806', 'P', '1', 'V806', '06');
insert into product_table(PRODUCT_NO, PRODUCT_CNAME, PRODUCT_VNAME, PRODUCT_ENAME, PRODUCT_KIND, BRAND_NO, PRODUCT_VNAME2, P_1) values('V807', 'C807', 'V807', 'E807', 'P', '1', 'V807', '06');
insert into product_table(PRODUCT_NO, PRODUCT_CNAME, PRODUCT_VNAME, PRODUCT_ENAME, PRODUCT_KIND, BRAND_NO, PRODUCT_VNAME2, P_1) values('V808', 'C808', 'V808', 'E808', 'P', '1', 'V808', '06');
insert into product_table(PRODUCT_NO, PRODUCT_CNAME, PRODUCT_VNAME, PRODUCT_ENAME, PRODUCT_KIND, BRAND_NO, PRODUCT_VNAME2, P_1) values('V809', 'C809', 'V809', 'E809', 'P', '1', 'V809', '06');


-- product_price table PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG
insert into PRODUCT_PRICE(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG) values('V801', 25, 230000, '2019.08.01','1');
insert into PRODUCT_PRICE(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG) values('V802', 25, 230000, '2019.08.01','1');
insert into PRODUCT_PRICE(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG) values('V803', 25, 230000, '2019.08.01','1');
insert into PRODUCT_PRICE(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG) values('V804', 25, 230000, '2019.08.01','1');
insert into PRODUCT_PRICE(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG) values('V805', 25, 230000, '2019.08.01','1');
insert into PRODUCT_PRICE(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG) values('V806', 25, 230000, '2019.08.01','1');
insert into PRODUCT_PRICE(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG) values('V807', 25, 230000, '2019.08.01','1');
insert into PRODUCT_PRICE(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG) values('V808', 25, 230000, '2019.08.01','1');
insert into PRODUCT_PRICE(PRODUCT_NO, PACKING_TYPE, UNIT_PRICE, USE_DATE, USE_FLAG) values('V809', 25, 230000, '2019.08.01','1');

-- BIG_KIND B_CODE, B_NAME, B_VNAME, B_ENAME, LOT_FLAG, STOCK_FLAG, PROCESS_PRICE, B_VNAME2
insert into BIG_KIND values('04', 'Heo', 'Heo', 'Pig',     'L', 'S', 10000, 'Heo2');
insert into BIG_KIND values('06', 'Ga',  'Ga',  'Chicken', 'L', 'S', 10000, 'Ga2');

-- devalue_help
Insert into devalue_help values('V801',25, '*','2019.09.01','2029.09.30',0.03, 3000);
Insert into devalue_help values('V801',25, '*','2019.09.01','2029.09.30',0.03, 3000);
Insert into devalue_help values('V802',25, '*','2019.09.01','2029.09.30',0.03, 3000);
Insert into devalue_help values('V802',25, '*','2019.09.01','2029.09.30',0.03, 3000);
Insert into devalue_help values('V803',25, '*','2019.09.01','2029.09.30',0.03, 3000);
Insert into devalue_help values('V803',25, '*','2019.09.01','2029.09.30',0.03, 3000);
Insert into devalue_help values('V804',25, '*','2019.09.01','2029.09.30',0.03, 3000);
Insert into devalue_help values('V804',25, '*','2019.09.01','2029.09.30',0.03, 3000);
Insert into devalue_help values('V805',25, '*','2019.09.01','2029.09.30',0.03, 3000);
Insert into devalue_help values('V805',25, '*','2019.09.01','2029.09.30',0.03, 3000);
Insert into devalue_help values('V806',25, '*','2019.09.01','2029.09.30',0.03, 3000);
Insert into devalue_help values('V807',25, '*','2019.09.01','2029.09.30',0.03, 3000);
Insert into devalue_help values('V808',25, '*','2019.09.01','2029.09.30',0.03, 3000);
Insert into devalue_help values('V809',25, '*','2019.09.01','2029.09.30',0.03, 3000);
Insert into devalue_help values('V810',25, '*','2019.09.01','2029.09.30',0.03, 3000);


-- trans_cost_table

Insert into trans_cost_table values('PSG','Kho Sai Gon',    200, 100, '1', '2019.08.01');
Insert into trans_cost_table values('PDN','Kho Dong Nai',   200, 100, '1', '2019.08.01');
Insert into trans_cost_table values('PTN','Kho Tay Nguyen', 200, 100, '1', '2019.08.01');



-- customer

-- CUST_NO, CUST_VNAME, PASSWORD, LABEL_FLAG, IMEI, CUST_VNAME2
Insert into customer values('C-001','Nguyen Van A', 'password1', '1', '', 'Nguyen Van A');
Insert into customer values('C-002','Nguyen Van B', 'password2', '2', '', 'Nguyen Van B');
Insert into customer values('C-003','Nguyen Van C', 'password3', '3', '', 'Nguyen Van C');
Insert into customer values('C-004','Nguyen Van D', 'password4', '5', '', 'Nguyen Van D');
Insert into customer values('C-005','Nguyen Van E', 'password5', 'R', '', 'Nguyen Van E');
Insert into customer values('C-006','Nguyen Van F', 'password6', 'R', '', 'Nguyen Van F');
Insert into customer values('C-007','Nguyen Van G', 'password7', '1', '', 'Nguyen Van G');


-- transport_table
-- (TRANSPORT_NO) CUST_NO, PLATE_NO, TRANSPORT_NAME, DRIVER_NAME, MOBILE

Insert into transport_table values('C-001', '50A 12.001', 'Huyndai', 'Tran Van A', '0918112201');
Insert into transport_table values('C-002', '50A 12.002', 'Huyndai', 'Tran Van B', '0918112202');
Insert into transport_table values('C-003', '50A 12.003', 'Huyndai', 'Tran Van C', '0918112203');
Insert into transport_table values('C-004', '50A 12.004', 'Huyndai', 'Tran Van D', '0918112204');
Insert into transport_table values('C-005', '50A 12.005', 'Huyndai', 'Tran Van E', '0918112205');
Insert into transport_table values('C-006', '50A 12.006', 'Huyndai', 'Tran Van F', '0918112206');
Insert into transport_table values('C-007', '50A 12.007', 'Huyndai', 'Tran Van G', '0918112207');
Insert into transport_table values('C-001', '50A 12.011', 'Huyndai', 'Tran Van 1', '0918112208');
Insert into transport_table values('C-001', '50A 12.012', 'Huyndai', 'Tran Van 2', '0918112209');


-- Order Master
-- ORDER_NO,ORDER_DATE,PLATE_NO,PLACE_NO,CUST_NO,OK_FLAG,CREATE_DATE,CONFIRM_DATE,CONFIRM_USER,FINISH_DATE

Insert into MER_DB.ORDER_MASTER values ('OR-001',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-001','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('11/OCT/19','DD/MON/RR'));
Insert into MER_DB.ORDER_MASTER values ('OR-002',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-002','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('12/OCT/19','DD/MON/RR'));
Insert into MER_DB.ORDER_MASTER values ('OR-003',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-003','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('13/OCT/19','DD/MON/RR'));
Insert into MER_DB.ORDER_MASTER values ('OR-004',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-001','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('14/OCT/19','DD/MON/RR'));
Insert into MER_DB.ORDER_MASTER values ('OR-005',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-002','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('15/OCT/19','DD/MON/RR'));
Insert into MER_DB.ORDER_MASTER values ('OR-006',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-003','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('16/OCT/19','DD/MON/RR'));

Insert into MER_DB.ORDER_MASTER values ('OR-007',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-001','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('17/OCT/19','DD/MON/RR'));
Insert into MER_DB.ORDER_MASTER values ('OR-008',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-001','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('18/OCT/19','DD/MON/RR'));
Insert into MER_DB.ORDER_MASTER values ('OR-009',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-001','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('19/OCT/19','DD/MON/RR'));
Insert into MER_DB.ORDER_MASTER values ('OR-010',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-001','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('20/OCT/19','DD/MON/RR'));
Insert into MER_DB.ORDER_MASTER values ('OR-011',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-001','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('21/OCT/19','DD/MON/RR'));
Insert into MER_DB.ORDER_MASTER values ('OR-012',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-001','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('22/OCT/19','DD/MON/RR'));
Insert into MER_DB.ORDER_MASTER values ('OR-013',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-001','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('23/OCT/19','DD/MON/RR'));
Insert into MER_DB.ORDER_MASTER values ('OR-014',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-001','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('24/OCT/19','DD/MON/RR'));
Insert into MER_DB.ORDER_MASTER values ('OR-015',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-001','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('25/OCT/19','DD/MON/RR'));
Insert into MER_DB.ORDER_MASTER values ('OR-016',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-001','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('26/OCT/19','DD/MON/RR'));
Insert into MER_DB.ORDER_MASTER values ('OR-017',to_date('10/OCT/19','DD/MON/RR'),'79A 123.12','PL1','C-001','1',to_date('10/OCT/19','DD/MON/RR'),to_date('10/OCT/19','DD/MON/RR'),'U123',to_date('27/OCT/19','DD/MON/RR'));


-- Order Details
-- ORDER_NO,S_NO,PRODUCT_NO,PACKING_TYPE,QTY,UNIT_PRICE

Insert into MER_DB.ORDER_DETAIL values ('OR-001',1,'V801',25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-002',1,'V801',25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-003',1,'V801',25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-001',2,'V802',25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-002',2,'V802',25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-003',2,'V802',25,1,1000);

Insert into MER_DB.ORDER_DETAIL values ('OR-001',3, 'V803', 25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-001',4, 'V804', 25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-001',5, 'V805', 25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-001',6, 'V806', 25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-001',7, 'V807', 25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-001',8, 'V808', 25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-001',9, 'V809', 25,1,1000);
-- wrong format below
Insert into MER_DB.ORDER_DETAIL values ('OR-001',10,'V8010',25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-001',11,'V8011',25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-001',12,'V8012',25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-001',13,'V8013',25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-001',14,'V8014',25,1,1000);
Insert into MER_DB.ORDER_DETAIL values ('OR-001',15,'V8015',25,1,1000);

