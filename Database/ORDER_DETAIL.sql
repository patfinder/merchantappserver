--------------------------------------------------------
--  File created - Thursday-October-10-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table ORDER_DETAIL
--------------------------------------------------------

  CREATE TABLE "MER_DB"."ORDER_DETAIL" 
   (	"ORDER_NO" VARCHAR2(12 BYTE), 
	"S_NO" NUMBER(5,0), 
	"PRODUCT_NO" VARCHAR2(20 BYTE), 
	"PACKING_TYPE" NUMBER(6,3), 
	"QTY" NUMBER(15,2), 
	"UNIT_PRICE" NUMBER(15,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table ORDER_DETAIL
--------------------------------------------------------

  ALTER TABLE "MER_DB"."ORDER_DETAIL" MODIFY ("ORDER_NO" NOT NULL ENABLE);
  ALTER TABLE "MER_DB"."ORDER_DETAIL" MODIFY ("S_NO" NOT NULL ENABLE);
  ALTER TABLE "MER_DB"."ORDER_DETAIL" MODIFY ("PRODUCT_NO" NOT NULL ENABLE);
  ALTER TABLE "MER_DB"."ORDER_DETAIL" MODIFY ("PACKING_TYPE" NOT NULL ENABLE);
  ALTER TABLE "MER_DB"."ORDER_DETAIL" MODIFY ("QTY" NOT NULL ENABLE);
  ALTER TABLE "MER_DB"."ORDER_DETAIL" MODIFY ("UNIT_PRICE" NOT NULL ENABLE);
