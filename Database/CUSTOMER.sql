--------------------------------------------------------
--  DDL for Table CUSTOMER
--------------------------------------------------------

  CREATE TABLE "MER_DB"."CUSTOMER" 
   (	"CUST_NO" VARCHAR2(8 BYTE), 
	"CUST_VNAME" NVARCHAR2(250), 
    "PASSWORD" NVARCHAR2(20), 
	"LABEL_FLAG" VARCHAR2(1 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table CUSTOMER
--------------------------------------------------------

  ALTER TABLE "MER_DB"."CUSTOMER" MODIFY ("CUST_NO" NOT NULL ENABLE);

--------------------------------------------------------
-- Nov 19

  ALTER TABLE CUSTOMER ADD IMEI VARCHAR2(30);
--  ALTER TABLE CUSTOMER DROP COLUMN IMEI;

-- Nov 20
ALTER TABLE CUSTOMER ADD CUST_VNAME2 NVARCHAR2(120);
