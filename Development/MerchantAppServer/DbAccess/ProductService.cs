﻿using Dapper;
using log4net;
using MerchantAppServer.Constants;
using MerchantAppServer.Models;
using MerchantAppServer.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MerchantAppServer.DbAccess
{
    public static class ProductService
    {
        static readonly ILog Log = LogManager.GetLogger(typeof(ProductService));

        /// <summary>
        /// Get products including products with discount
        /// </summary>
        /// <param name="custNo"></param>
        /// <returns></returns>
        public static List<Devalue> GetCustomerProducts(string custNo, string labelFlag, DateTime ViewDate = default(DateTime), string[] productNos = null)
        {
            using (var conn = DbUtils.Connection)
            {
                var productList = "";
                if (ViewDate == default(DateTime)) ViewDate = DateTime.Now;

                if (productNos?.Length > 0)
                {
                    var productNos2 = productNos.Select(p => $"'{p}'");
                    productList = $"AND P.PRODUCT_NO IN ({string.Join(", ", productNos2)})";
                }

                var sql = DbUtils._R(
                    "SELECT P.P_1 P1, P.PRODUCT_NO ProductNo, P.PRODUCT_VNAME2 ProductVName, R.PACKING_TYPE PackingType, R.UNIT_PRICE UnitPrice, " +
                    "    SUM(COALESCE(D.PERCENT, 0)) Percent, SUM(COALESCE(D.DISCOUNT, 0)) Discount " +
                    "FROM PRODUCT_TABLE P  " +
                    "    INNER JOIN PRODUCT_PRICE R ON (P.PRODUCT_NO = R.PRODUCT_NO AND R.USE_FLAG = '1') " +
                    "    LEFT JOIN DEVALUE_HELP D ON R.PRODUCT_NO = D.PRODUCT_NO1 AND R.PACKING_TYPE = D.PACKING_TYPE " +
                    "WHERE P.PRODUCT_KIND = 'P' " +
                    productList +
                    "    AND (P.BRAND_NO = :LabelFlag OR P.BRAND_NO NOT IN ('1','2','3','5')) " +
                    "    AND ( " +
                    "        D.PRODUCT_NO1 IS NULL  " +
                    "        OR ((D.CUST_NO = '*' OR D.CUST_NO = :CustomerNo) " +
                    "            AND :ViewDate BETWEEN F_DATE AND T_DATE) " +
                    "    ) " +
                    "GROUP BY P.P_1, P.PRODUCT_NO, P.PRODUCT_VNAME2, R.PACKING_TYPE, R.UNIT_PRICE " +
                    "ORDER BY P.PRODUCT_VNAME2", new
                    {
                        CustomerNo = custNo,
                        LabelFlag = labelFlag,
                        ViewDate = DbUtils._ToDbDate(ViewDate),
                    });

                //Log.Debug($"{nameof(GetCustomerProducts)} sql: {sql}");

                var data = conn.Query<Devalue>(sql).ToList();

                return data;
            }
        }
    }
}
