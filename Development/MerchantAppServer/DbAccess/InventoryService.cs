﻿using Dapper;
using MerchantAppServer.Constants;
using MerchantAppServer.Models;
using MerchantAppServer.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

namespace MerchantAppServer.DbAccess
{
    public static class InventoryService
    {
        public static IEnumerable<Inventory> Get()
        {
            var inventoryStr = ConfigurationManager.AppSettings[$"Inventories"];
            var inventoryList = inventoryStr
                .Split(new[] { "-**-" }, StringSplitOptions.RemoveEmptyEntries)
                .Select(item => new {item, parts = item.Split(new[] { '-' }, 2)})
                .Select(bucket => new Inventory
                {
                    InventoryNo = bucket.parts[0].Trim(),
                    InventoryName = bucket.item,
                });

            return inventoryList;
        }
    }
}
