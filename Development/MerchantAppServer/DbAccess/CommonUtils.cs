﻿using Dapper;
using MerchantAppServer.Constants;
using MerchantAppServer.Models;
using MerchantAppServer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MerchantAppServer.DbAccess
{
    public static class CommonUtils
    {
        public static string IdFromTime(DateTime time)
        {
            return time.ToString(DbUtils.TimeStampIdFormat);
        }

        public static string ShortIdFromDate(DateTime date)
        {
            return date.Date.ToString(DbUtils.ShortTimeIdStampFormat);
        }
    }
}
