﻿using Dapper;
using MerchantAppServer.Constants;
using MerchantAppServer.Models;
using MerchantAppServer.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MerchantAppServer.DbAccess
{
    public static class TransportService
    {
        public static TransportInfo Get(string plateNo)
        {
            using (var conn = DbUtils.Connection)
            {
                var fieldList = new[]{
                    //nameof(TransportInfo.TransportNo  ),
                    nameof(TransportInfo.CustNo       ),
                    nameof(TransportInfo.PlateNo      ),
                    nameof(TransportInfo.TransportName),
                    nameof(TransportInfo.DriverName   ),
                    nameof(TransportInfo.Mobile       ),
                    //nameof(TransportInfo.TrailerPlate ),
                };
                var fieldStr = string.Join(", ", fieldList.Select(DbUtils._ToDbName));

                return conn.Query<TransportInfo>(DbUtils._T(
                    $"SELECT {fieldStr} FROM TRANSPORT_TABLE " +
                    "WHERE PLATE_NO = #PLATENO AND ROWNUM = 1"),
                    new { plateNo }).FirstOrDefault();
            }
        }

        public static List<TransportInfo> GetCustomerTransports(string custNo)
        {
            using (var conn = DbUtils.Connection)
            {
                var fieldList = new[]{
                    //nameof(TransportInfo.CustNo       ),
                    nameof(TransportInfo.PlateNo      ),
                    nameof(TransportInfo.TransportName),
                    nameof(TransportInfo.DriverName   ),
                    nameof(TransportInfo.Mobile       ),
                    //nameof(TransportInfo.TrailerPlate ),
                };
                var fieldStr = string.Join(", ", fieldList.Select(f => DbUtils._ToDbName(f) + " " + f));

                return conn.Query<TransportInfo>(DbUtils._T(
                    $"SELECT {fieldStr} FROM TRANSPORT_TABLE " +
                    "WHERE CUST_NO = #CUSTNO"),
                    new { custNo }).ToList();
            }
        }

        public static string CreateTransport(TransportInfo transport)
        {
            //var transportNo = CommonUtils.ShortIdFromDate(DateTime.Now);

            var paramObj = new
            {
                //transportNo,
                transport.CustNo, // Initialized by caller
                transport.PlateNo,
                transport.TransportName,
                transport.DriverName,
                transport.Mobile,
                //transport.TransportName,
            };

            var fieldList = new[]{
                //nameof(TransportInfo.TransportNo),
                nameof(TransportInfo.CustNo),
                nameof(TransportInfo.PlateNo),
                nameof(TransportInfo.TransportName),
                nameof(TransportInfo.DriverName),
                nameof(TransportInfo.Mobile),
                //nameof(TransportInfo.TransportName),
            };

            var fieldStr = string.Join(", ", fieldList.Select(DbUtils._ToDbName));
            var fieldValueStr = string.Join(", ", fieldList.Select(f => $"#{f}"));

            var resultCount = DbUtils.ExecuteNonQuery(
                DbUtils._R($"INSERT INTO TRANSPORT_TABLE({fieldStr}) VALUES({fieldValueStr})", paramObj)
            );

            //return paramObj.Get<string>("RETURN_ID");
            return transport.PlateNo;
        }
    }
}
