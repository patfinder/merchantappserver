﻿using Dapper;
using MerchantAppServer.Constants;
using MerchantAppServer.Models;
using MerchantAppServer.Utils;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;

namespace MerchantAppServer.DbAccess
{
    public static class OrderService
    {
        public static string CreateOrderMaster(OrderMaster order)
        {
            var createTime = DateTime.Now;
            var orderNo = CommonUtils.IdFromTime(createTime);

            if(order == null)
            {
                throw new InvalidOperationException("Please provide correct input");
            }

            var paramObj = new
            {
                orderNo,
                orderDate = createTime,
                order.PlateNo,
                order.PlaceNo, // Store Id
                order.CustNo,  // Initialized by caller
                okFlag = OrderStatus.New,
                createDate = createTime,
                ConfirmDate = DateTime.MinValue,
                ConfirmUser = "U000",
                FinishDate = DateTime.MinValue,
            };

            var fieldList = new[]{
                nameof(OrderMaster.OrderNo),
                nameof(OrderMaster.OrderDate),
                nameof(OrderMaster.PlateNo),
                nameof(OrderMaster.PlaceNo),
                nameof(OrderMaster.CustNo),
                nameof(OrderMaster.OkFlag),
                nameof(OrderMaster.CreateDate),
                nameof(OrderMaster.ConfirmDate),
                nameof(OrderMaster.ConfirmUser),
                nameof(OrderMaster.FinishDate),
            };

            var fieldStr = string.Join(", ", fieldList.Select(f => DbUtils._ToDbName(f)));
            var fieldValueStr = string.Join(", ", fieldList.Select(f => $"#{f}"));
            var sql = DbUtils._R($"INSERT INTO ORDER_MASTER({fieldStr}) VALUES({fieldValueStr})", paramObj);

            var resultCount = DbUtils.ExecuteNonQuery(sql);

            return resultCount > 0 ? orderNo : null;
        }

        public static int CreateOrderDetails(IEnumerable<OrderDetail> details)
        {
            var fieldList = new[]{
                nameof(OrderDetail.OrderNo    ),
                nameof(OrderDetail.ProductNo  ),
                nameof(OrderDetail.PackingType),
                nameof(OrderDetail.Qty        ),
                nameof(OrderDetail.UnitPrice  ),
                //nameof(OrderDetail.SerialNo   ),
            };

            var fieldStr = string.Join(", ", fieldList.Select(f => DbUtils._ToDbName(f)).Append("S_NO"));
            var fieldValueStr = string.Join(", ", fieldList.Select(f => $"#{f}").Append("#SerialNo"));

            var totalCount = 0;
            details.Select((detail, idx) => new { detail, idx }).ToList().ForEach(d2 =>
            {
                d2.detail.SerialNo = (d2.idx + 1).ToString("D2", CultureInfo.InvariantCulture);

                // Param obj
                //var paramss = new[] {
                //    DbUtils.CreateParameter(nameof(d2.detail.OrderNo),      OracleDbType.NVarchar2, d2.detail.OrderNo),
                //    DbUtils.CreateParameter(nameof(d2.detail.ProductNo),    OracleDbType.NVarchar2, d2.detail.ProductNo),
                //    DbUtils.CreateParameter(nameof(d2.detail.PackingType),  OracleDbType.Double,    d2.detail.PackingType),
                //    DbUtils.CreateParameter(nameof(d2.detail.Qty),          OracleDbType.Int16,     d2.detail.Qty),
                //    DbUtils.CreateParameter(nameof(d2.detail.UnitPrice),    OracleDbType.Double,    d2.detail.UnitPrice),
                //};

                var resultCount = DbUtils.ExecuteNonQuery(
                    DbUtils._R($"INSERT INTO ORDER_DETAIL({fieldStr}) VALUES({fieldValueStr})", d2.detail)
                );

                totalCount += resultCount;
            });

            return totalCount;
        }
    }
}
