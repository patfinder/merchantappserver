﻿using Dapper;
using MerchantAppServer.Constants;
using MerchantAppServer.Models;
using MerchantAppServer.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

namespace MerchantAppServer.DbAccess
{
    public static class BigKindService
    {
        public static List<BigKind> GetP1List()
        {
            var bigKindStr = ConfigurationManager.AppSettings[$"BigKinds"];
            var bigKindList = bigKindStr
                .Split(new[] { "-**-" }, StringSplitOptions.RemoveEmptyEntries)
                .Select(item => new { item, parts = item.Split(new[] { '-' }, 2) })
                .Select(bucket => new BigKind
                {
                    Code = bucket.parts[0].Trim(),
                    VName = bucket.parts[1].Trim(),
                })
                .ToList();

            return bigKindList;
        }
    }
}
