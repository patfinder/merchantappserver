﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
//using AttributeRouting.Web.Mvc;

namespace MerchantAppServer.Models
{
    public class TransportInfo
    {
        //public string TransportNo { get; set; }

        public string CustNo { get; set; }

        public string PlateNo { get; set; }

        public string TransportName { get; set; }

        public string DriverName { get; set; }

        public string TrailerPlate { get; set; }

        /// <summary>
        /// Max 10 chars
        /// </summary>
        public string Mobile { get; set; }

        //public Boolean IsNew { get; set; }
    }
}
