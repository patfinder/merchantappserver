﻿using MerchantAppServer.Constants;
using System;
//using AttributeRouting.Web.Mvc;

namespace MerchantAppServer.Models
{
    public interface IPagingModel
    {
        /// <summary>
        /// Page number, start from 1
        /// </summary>
        int Page { get; set; }

        int PageSize { get; set; }
    }

    public class PagingModelImpl : IPagingModel
    {
        public int Page { get; set; }

        public int PageSize { get; set; }

        public PagingModelImpl()
        {
            Page = 1;
            PageSize = SystemConstants.DefaultPageSize;
        }
    }
}
