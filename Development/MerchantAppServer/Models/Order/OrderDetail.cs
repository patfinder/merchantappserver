﻿using MerchantAppServer.Constants;
using System;
//using AttributeRouting.Web.Mvc;

namespace MerchantAppServer.Models
{
    public class OrderDetail
    {
        public string OrderNo { get; set; }

        public string SerialNo { get; set; }

        public string ProductNo { get; set; }

        public float PackingType { get; set; }

        public int Qty { get; set; }

        /// <summary>
        /// This is unit price minus discounts
        /// </summary>
        public float UnitPrice { get; set; }
    }
}
