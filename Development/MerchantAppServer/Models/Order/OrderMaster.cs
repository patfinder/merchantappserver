﻿using MerchantAppServer.Constants;
using System;
using System.Collections.Generic;
//using AttributeRouting.Web.Mvc;

namespace MerchantAppServer.Models
{
    public class OrderMaster
    {
        public string OrderNo { get; set; }
        public DateTime OrderDate { get; set; }

        public string PlateNo { get; set; }

        // Transport Info
        public string PlaceNo { get; set; }

        // Order summary
        // TODO: Order Total, Detail count
        public float TotalAmount { get; set; }

        public float TotalWeight { get; set; }

        // Customer Info
        public string CustNo { get; set; }
        public OrderStatus OkFlag { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ConfirmDate { get; set; }
        public string ConfirmUser { get; set; }
        public DateTime FinishDate { get; set; }

        // Transport
        public TransportInfo Transport { get; set; }

        // Order Items
        public List<OrderDetail> OrderDetails { get; set; }

        public OrderMaster()
        {
            OrderDetails = new List<OrderDetail>();
        }
    }

    public enum OrderStatus
    {
        New = 0,
        CustomerConfirmed = 1,
        CompanyConfirmed = 2,
        Processing = 3,
        Completed = 4,
        // Cancelled = 5 // TODO: ??
    }
}
