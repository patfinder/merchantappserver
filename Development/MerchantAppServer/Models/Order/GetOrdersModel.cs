﻿using MerchantAppServer.Constants;
using System;
using System.Collections.Generic;
//using AttributeRouting.Web.Mvc;

namespace MerchantAppServer.Models
{
    public class GetOrdersModel : IPagingModel
    {
        public int Page { get; set; }

        public int PageSize { get; set; }

        public IEnumerable<OrderMaster> Orders { get; set; }
    }
}
