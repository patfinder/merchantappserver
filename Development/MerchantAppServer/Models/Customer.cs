﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
//using AttributeRouting.Web.Mvc;

namespace MerchantAppServer.Models
{
    public class Customer
    {
        public string CustNo { get; set; }

        public string CustVName { get; set; }

        public string Password { get; set; }

        public string LabelFlag { get; set; }

        public Role Role { get; set; }

        // TODO: Monthly total
        public DateTime LastMonth { get; set; }

        public float LastMonthOrderCount { get; set; }

        public float LastMonthWeight { get; set; }
    }

    //[JsonConverter(typeof(StringEnumConverter))]
    public enum Role
    {
        Unknown,
        Customer,
    }

    public static class LabelFlag
    {
        public const string L1 = "1";
        public const string L2 = "2";
        public const string L3 = "3";
        //public const string L4 = "4";
        public const string L5 = "5";
        public const string LR = "R";
    }

    public static class CustomClaimTypes
    {
        public const string LabelFlag = "http://schemas.smartdev.com/ws/2019/12/identity/claims/labelflag";
    }

}
