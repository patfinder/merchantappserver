﻿using System;
//using AttributeRouting.Web.Mvc;

namespace MerchantAppServer.Models
{
    /// <summary>
    /// Discount
    /// </summary>
    public class Devalue
    {
        public string ProductNo { get; set; }

        public string P1 { get; set; }

        public float PackingType { get; set; }

        public float UnitPrice { get; set; }

        public string CustNo { get; set; }

        public string FDate { get; set; }

        public string TDate { get; set; }

        public float Percent { get; set; }

        public float Discount { get; set; }

        /// <summary>
        /// From Product table
        /// </summary>
        public string ProductVName { get; set; }

        /// <summary>
        /// Get discounted price. This require UnitPrice, (discount) Percent and Discount
        /// </summary>
        public float DiscountPrice => this.UnitPrice - (this.UnitPrice * this.Percent) - this.Discount;
    }
}
