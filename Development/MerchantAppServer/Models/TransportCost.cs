﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
//using AttributeRouting.Web.Mvc;

namespace MerchantAppServer.Models
{
    /// <summary>
    /// Transport cost model
    /// </summary>
    public class TransportCost
    {
        public string PlaceNo { get; set; }

        public string PlaceName2 { get; set; }

        public float LivestockVnd { get; set; }

        public float AquaVnd { get; set; }

        public string UseFlag { get; set; }

        public string TheDate { get; set; }
    }
}
