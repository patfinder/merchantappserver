﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MerchantAppServer.Models
{
    public class ApiResult
    {
        public ResultCode ResultCode { get; set; }

        public string[] Messages { get; set; }

        public IPagingModel Paging { get; set; }

        public object Data { get; set; }

        public ApiResult()
        {
            ResultCode = ResultCode.Success;
            Messages = new string[0];
        }

        public static ApiResult Succeeded(string message = null)
        {
            return new ApiResult()
            {
                Messages = message == null ? new string[0] : new[] { message },
            };
        }

        public static ApiResult Failed(string message)
        {
            return new ApiResult
            {
                ResultCode = ResultCode.GenericError,
                Messages = new[] { message },
            };
        }
    }

    //[JsonConverter(typeof(StringEnumConverter))]
    public enum ResultCode
    {
        Unknown = 0,
        Success = 1,
        GenericError,
        Unauthenticated,
    }
}
