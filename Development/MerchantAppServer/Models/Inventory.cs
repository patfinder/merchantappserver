﻿using MerchantAppServer.Constants;
using System;
//using AttributeRouting.Web.Mvc;

namespace MerchantAppServer.Models
{
    /// <summary>
    /// Thuốc/Thức ăn
    /// </summary>
    public class Inventory
    {
        public string InventoryNo { get; set; }

        public string InventoryName { get; set; }
    }
}
