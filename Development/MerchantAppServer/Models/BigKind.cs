﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
//using AttributeRouting.Web.Mvc;

namespace MerchantAppServer.Models
{
    public class BigKind
    {
        public string Code { get; set; }

        public string VName { get; set; }
    }
}
