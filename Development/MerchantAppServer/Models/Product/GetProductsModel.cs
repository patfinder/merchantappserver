﻿using MerchantAppServer.Constants;
using System;
using System.Collections.Generic;
//using AttributeRouting.Web.Mvc;

namespace MerchantAppServer.Models
{
    /// <summary>
    /// Thuốc/Thức ăn
    /// </summary>
    public class GetProductsModel : IPagingModel
    {
        public int Page { get; set; }

        public int PageSize { get; set; }

        public List<Product> Products { get; set; }
    }
}
