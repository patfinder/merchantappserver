﻿using MerchantAppServer.Constants;
using System;
//using AttributeRouting.Web.Mvc;

namespace MerchantAppServer.Models
{
    /// <summary>
    /// Thuốc/Thức ăn
    /// </summary>
    public class Product
    {
        public string ProductNo { get; set; }

        public float PackingType { get; set; }

        public float UnitPrice { get; set; }

        public string UseDate { get; set; }

        public string UseFlag { get; set; }

        public string ProductVName { get; set; }

        public string ProductCName { get; set; }

        public string ProductEName { get; set; }

        public float ProductKind { get; set; }
    }
}
