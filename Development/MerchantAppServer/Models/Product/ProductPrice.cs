﻿using MerchantAppServer.Constants;
using System;
//using AttributeRouting.Web.Mvc;

namespace MerchantAppServer.Models
{
    /// <summary>
    /// Bảng giá Sản phẩm
    /// </summary>
    public class ProductPrice
    {
        public string ProductNo { get; set; }

        public float PackingType { get; set; }

        public float UnitPrice { get; set; }

        public string UseDate { get; set; }

        public string UseFlag { get; set; }
    }
}
