﻿namespace MerchantAppServer.Models
{
    public class LoginModel
    {
        public string CustomerNo { get; set; }

        public string Password { get; set; }

        public string Imei { get; set; }
    }
}
