﻿using MerchantAppServer.Models;
using log4net;
using Microsoft.Owin.Security;
using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Mvc;
using Oracle.ManagedDataAccess.Client;
using AttributeRouting.Web.Mvc;
using Dapper;
using MerchantAppServer.Utils;
using MerchantAppServer.Constants;
using System.IO;
using MerchantAppServer.DbAccess;

namespace MerchantAppServer.Controllers
{
    /// <summary>
    /// Product management APIs
    /// </summary>
    [Authorize, ActionLog, ExceptionLog]
    public class ProductController : BaseController
    {
        /// <summary>
        /// Get products including products with discount
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                return Json(new ApiResult
                {
                    Data = ProductService.GetCustomerProducts(CustomerNo, LabelFlag)
                        .Select(item => item.ToCamelCaseObject()),
                }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                Log.Error(ex.Message, ex);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Internal server error");
            }
        }
    }
}
