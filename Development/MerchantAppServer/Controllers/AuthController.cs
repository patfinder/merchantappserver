﻿using MerchantAppServer.Models;
using log4net;
using Microsoft.Owin.Security;
using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Mvc;
using Oracle.ManagedDataAccess.Client;
using Dapper;
using MerchantAppServer.Utils;
using MerchantAppServer.Constants;
using MerchantAppServer.Controllers;
using Newtonsoft.Json;

namespace MerchantAppServer.Controllers
{
    /// <summary>
    /// User and Authentication APIs
    /// </summary>
    [Authorize, ActionLog, ExceptionLog]
    public class AuthController : BaseController // BaseController Controller
    {
        protected static readonly ILog Log = LogManager.GetLogger(typeof(AuthController));

        [HttpPost, AllowAnonymous] // HttpGet, 
        public ActionResult Login(LoginModel model)
        {
            Log.Info($"Login. model param: {JsonConvert.SerializeObject(model)}");

            using (var conn = DbUtils.Connection)
            {
                var command = new CommandDefinition(
                    DbUtils._T(
                        "SELECT CUST_NO CustNo, CUST_VNAME CustVName, LABEL_FLAG LabelFlag FROM CUSTOMER " +
                        "WHERE CUST_NO = #CustomerNo AND PASSWORD = #Password AND (IMEI IS NULL OR IMEI = #Imei) "),
                    new { model.CustomerNo, model.Password, model.Imei }
                );
                // TOD_: TEST
                //var command = new CommandDefinition(
                //    DbUtils._T("SELECT CUST_NO CustNo, CUST_VNAME CustVName FROM CUSTOMER WHERE CUST_NO = #CUSTOMERNO"),
                //    new { model.CustomerNo, model.Password }
                //);

                var user = conn.Query<Customer>(command).FirstOrDefault();

                if (user == null)
                {
                    return new HttpNotFoundResult("Invalid name or password");
                }

                var identity = new ClaimsIdentity(new[]{
                    new Claim(ClaimTypes.Name, user.CustVName),
                    new Claim(ClaimTypes.NameIdentifier, user.CustNo),
                    new Claim(CustomClaimTypes.LabelFlag, user.LabelFlag),
                }, SystemConstants.AuthenticationCookie);

                Authentication.SignIn(new AuthenticationProperties
                {
                    IsPersistent = true, // Remember Me
                }, identity);

                return Json(new ApiResult
                {
                    Data = new
                    {
                        user.CustNo,
                        user.CustVName,
                        user.Role,
                    }.ToCamelCaseObject()
                });
            }
        }

        /// <summary>
        /// This API is for checking current user session token
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult TestLogin()
        {
            if(Authentication.User.Identity.IsAuthenticated)
            {
                return Json("CheckLogin: Authenticated user.", JsonRequestBehavior.AllowGet);
            }

            return Json(new ApiResult {
                ResultCode = ResultCode.Unauthenticated,
                Messages = new[] { "CheckLogin: Unauthenticated user." },
            }, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult TestAnonymous()
        {
            //return new HttpStatusCodeResult(HttpStatusCode.OK, "Anonymous User");
            return Json("CheckAnonymous successfully.", JsonRequestBehavior.AllowGet);
        }
    }
}
