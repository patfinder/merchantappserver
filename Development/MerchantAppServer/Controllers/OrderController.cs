﻿using MerchantAppServer.Models;
using log4net;
using Microsoft.Owin.Security;
using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Mvc;
using Oracle.ManagedDataAccess.Client;
using AttributeRouting.Web.Mvc;
using Dapper;
using MerchantAppServer.Utils;
using MerchantAppServer.Constants;
using System.IO;
using System.Transactions;
using MerchantAppServer.DbAccess;
using Newtonsoft.Json;

namespace MerchantAppServer.Controllers
{
    /// <summary>
    /// Order management APIs
    /// </summary>
    [Authorize, ActionLog, ExceptionLog]
    public class OrderController : BaseController
    {
        [HttpGet]
        public ActionResult GetCreateOrderInfo()
        {
            // TOD_: TEST
            //Authentication.SignOut();

            return Json(new ApiResult
            {
                Data = new
                {
                    InventoryList = InventoryService.Get()
                        .Select(item => item.ToCamelCaseObject()),
                    BigKinds = BigKindService.GetP1List()
                        .Select(item => item.ToCamelCaseObject()),
                    Products = ProductService.GetCustomerProducts(CustomerNo, LabelFlag)
                        .Select(item => item.ToCamelCaseObject()),
                    Transports = TransportService.GetCustomerTransports(CustomerNo)
                        .Select(item => item.ToCamelCaseObject()),
                    // TODO
                    ThisMonthOrderCount = 0,
                    ThisMonthOrderAmount = 0.0,
                }
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Get(IPagingModel paging)
        {
            if(!(paging?.Page >= 1 && paging?.PageSize >= 1))
            {
                throw new ArgumentException("Invalid Page or PageSize");
            }

            using(var conn = DbUtils.Connection)
            {
                // this month
                //var today = DateTime.Now;
                //var thisMonth = new DateTime(today.Year, today.Month, 1);

                var paramObj = new
                {
                    CustomerNo,
                    paging.Page,
                    paging.PageSize,
                };

                var fieldList = new[]{
                    nameof(OrderMaster.OrderNo    ),
                    nameof(OrderMaster.OrderDate  ),
                    nameof(OrderMaster.PlateNo    ),
                    nameof(OrderMaster.PlaceNo    ),
                    nameof(OrderMaster.CustNo     ),
                    nameof(OrderMaster.OkFlag     ),
                    nameof(OrderMaster.CreateDate ),
                    nameof(OrderMaster.ConfirmDate),
                    nameof(OrderMaster.ConfirmUser),
                    nameof(OrderMaster.FinishDate ),
                };

                var fieldsStr = string.Join(", ", fieldList.Select(f => DbUtils._ToDbName(f) + " " + f));
                var innerFieldsStr = string.Join(", ", fieldList.Select(f => DbUtils._ToDbName(f)));

                var orders = conn.Query<OrderMaster>(DbUtils._R(
                    $"SELECT {fieldsStr} " +
                    $"FROM ( " +
                    $"    SELECT {innerFieldsStr}, ROWNUM RN" +
                    $"    FROM ( " +
                    $"        SELECT {innerFieldsStr} FROM ORDER_MASTER WHERE CUST_NO = '#CUSTOMERNO' " +
                    $"        ORDER BY CREATE_DATE DESC, ORDER_NO DESC " +
                    $"    ) Q1 " +
                    $"    WHERE ROWNUM < #PAGE * #PAGESIZE + 1 " +
                    $") " +
                    $"WHERE RN >= ((#PAGE - 1) * #PAGESIZE) + 1 ",
                    paramObj)
                );

                return Json(new ApiResult
                {
                    Data = new {
                        paging.Page,
                        paging.PageSize,
                        orders,
                    }.ToCamelCaseObject(),
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetPreviousOrders(/*string date*/ DateTime viewMonth)
        {
            //DateTime viewMonth = DateTime.Parse(date);

            var date1 = new DateTime(viewMonth.Year, viewMonth.Month, 1);
            var date2 = date1.AddMonths(1);

            using(var conn = DbUtils.Connection)
            {
                var paramObj = new
                {
                    CustomerNo, date1, date2
                };

                var fieldList = new[]{
                    nameof(OrderMaster.OrderNo    ),
                    nameof(OrderMaster.OrderDate  ),
                    nameof(OrderMaster.PlateNo    ),
                    nameof(OrderMaster.PlaceNo    ),
                    nameof(OrderMaster.CustNo     ),
                    nameof(OrderMaster.OkFlag     ),
                    nameof(OrderMaster.CreateDate ),
                    nameof(OrderMaster.ConfirmDate),
                    nameof(OrderMaster.ConfirmUser),
                    nameof(OrderMaster.FinishDate ),
                };

                var fieldsStr = string.Join(", ", fieldList.Select(f => DbUtils._ToDbName(f) + " " + f));

                var orders = conn.Query<OrderMaster>(DbUtils._R(
                    $"SELECT {fieldsStr} " +
                    $"FROM ORDER_MASTER " +
                    $"WHERE CUST_NO = #CUSTOMERNO " +
                    $"	AND #DATE1 <= ORDER_DATE AND ORDER_DATE < #DATE2 " +
                    $"ORDER BY CREATE_DATE DESC, ORDER_NO DESC",
                    paramObj)
                ).ToList();

                var detailFieldList = new[]{
                    nameof(OrderDetail.OrderNo      ),
                    nameof(OrderDetail.ProductNo    ),
                    nameof(OrderDetail.PackingType  ),
                    //nameof(OrderDetail.UnitPrice    ),
                    nameof(OrderDetail.Qty          ),
                };
                var detailFieldsStr = string.Join(", ", detailFieldList.Select(f => DbUtils._ToDbName(f) + " " + f));


                // Calculate total amount / weight
                for(var i=0; i<orders.Count; i++)
                {
                    var order = orders[i];

                    // Get order details
                    var orderNos = string.Join(", ", orders.Select(o => $"'{o.OrderNo}'"));
                    var details = new List<OrderDetail>();

                    if (!string.IsNullOrEmpty(orderNos))
                    {
                        details = conn.Query<OrderDetail>(
                            $"SELECT {detailFieldsStr} " +
                            $"FROM ORDER_DETAIL " +
                            $"WHERE ORDER_NO IN ({orderNos})"
                        ).ToList();
                    }

                    // Get product info
                    var productNos = details.Select(o => o.ProductNo).Distinct().ToArray();
                    var products = ProductService.GetCustomerProducts(CustomerNo, LabelFlag, order.CreateDate, productNos);

                    // Update total
                    details.Where(d => d.OrderNo == order.OrderNo).ToList().ForEach(detail =>
                    {
                        // Get price with discount
                        var product = products.Where(p => p.ProductNo == detail.ProductNo && p.PackingType == detail.PackingType).FirstOrDefault();

                        if (product != null)
                        {
                            order.TotalWeight += detail.PackingType * detail.Qty;
                            order.TotalAmount += product.DiscountPrice * detail.Qty;

                            //Log.Debug($"Product: {detail.ProductNo}, {order.TotalWeight}, {order.TotalAmount}");
                        }
                        else
                        {
                            // Unexpected case
                            Log.Error($"{nameof(OrderController)}.{nameof(OrderController.GetPreviousOrders)} error: " +
                                $"Product info for product {detail.ProductNo}, {detail.PackingType}kg is not found.");
                        }
                    });
                }

                return Json(new ApiResult
                {
                    Data = new
                    {
                        orders,
                    }.ToCamelCaseObject(),
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Create(OrderMaster order)
        {
            try
            {
                Log.Info($"Create. order param: {JsonConvert.SerializeObject(order)}");

                if (order.OrderDetails.Count <= 0)
                {
                    throw new InvalidOperationException("OrderDetails must have one item or more.");
                }

                // Search/Create transport
                if (!string.IsNullOrEmpty(order.PlateNo))
                {
                    var transport = TransportService.Get(order.PlateNo);
                    if (transport == null) throw new Exception($"{order.PlateNo} not found.");
                }
                else if(!string.IsNullOrEmpty(order.Transport.PlateNo))
                {
                    order.Transport.CustNo = CustomerNo;
                    order.PlateNo = order.Transport.PlateNo;

                    TransportService.CreateTransport(order.Transport);
                }

                // Create Order master
                order.CustNo = CustomerNo;

                // Create Order Master
                var orderNo = OrderService.CreateOrderMaster(order);

                // Create Order failed
                if (orderNo == null)
                {
                    return Json(ApiResult.Failed("Failed to create order."), JsonRequestBehavior.AllowGet);
                }

                // Create Order details
                order.OrderDetails.ToList().ForEach(detail => detail.OrderNo = orderNo);

                if (OrderService.CreateOrderDetails(order.OrderDetails) != order.OrderDetails.Count)
                {
                    return Json(ApiResult.Failed("Failed to create one or more Order details."), JsonRequestBehavior.AllowGet);
                }

                return Json(ApiResult.Succeeded());
            }
            catch(Exception ex)
            {
                Log.Error($"{nameof(OrderController)}.Create error: {ex.Message}", ex);
                return Json(ApiResult.Failed(ex.Message));
            }
        }
    }
}
