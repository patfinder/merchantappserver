﻿using MerchantAppServer.Models;
using log4net;
using Microsoft.Owin.Security;
using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Mvc;
using Oracle.ManagedDataAccess.Client;
using AttributeRouting.Web.Mvc;
using Dapper;
using MerchantAppServer.Utils;
using MerchantAppServer.Constants;
using System.IO;
using System.Configuration;
using MerchantAppServer.DbAccess;

namespace MerchantAppServer.Controllers
{
    /// <summary>
    /// Inventory (Kho) APIs
    /// </summary>
    [AllowAnonymous, ActionLog, ExceptionLog]
    public class InventoryController : BaseController
    {
        /// <summary>
        /// Get Inventories list from Server setting
        /// </summary>
        /// <returns></returns>
        public ActionResult Get()
        {
            return Json(new ApiResult
            {
                Data = InventoryService.Get().Select(item => item.ToCamelCaseObject()),
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
