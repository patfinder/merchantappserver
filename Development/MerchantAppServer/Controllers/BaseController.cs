﻿using log4net;
using MerchantAppServer.Models;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace MerchantAppServer.Controllers
{
    public class BaseController : Controller
    {
        protected static readonly ILog Log = LogManager.GetLogger(typeof(BaseController));

        protected IAuthenticationManager Authentication => HttpContext.GetOwinContext().Authentication;

        protected string CustomerNo => Authentication.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;

        protected string LabelFlag => Authentication.User.Claims.FirstOrDefault(c => c.Type == CustomClaimTypes.LabelFlag)?.Value;

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonDotNetResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior
            };
        }
    }
}
