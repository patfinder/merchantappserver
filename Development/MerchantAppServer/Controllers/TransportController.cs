﻿using MerchantAppServer.Models;
using log4net;
using Microsoft.Owin.Security;
using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Mvc;
using Oracle.ManagedDataAccess.Client;
using AttributeRouting.Web.Mvc;
using Dapper;
using MerchantAppServer.Utils;
using MerchantAppServer.Constants;
using System.IO;

namespace MerchantAppServer.Controllers
{
    /// <summary>
    /// Transport (Phương tiện) management APIs
    /// </summary>
    [Authorize, ActionLog, ExceptionLog]
    public class TransportController : BaseController
    {
        /// <summary>
        /// Get products with discount
        /// </summary>
        /// <returns></returns>
        public ActionResult Get()
        {
            throw new NotImplementedException("TransportController.Get");

            try
            {
                //using (var conn = DbUtils.Connection)
                //{
                //    var data = conn.Query(DbUtils._T(
                //        "SELECT P.PRODUCT_NO, P.PACKING_TYPE, P.UNIT_PRICE, P.USE_DATE, P.USE_FLAG, " +
                //        "    D.PERCENT, D.DISCOUNT " +
                //        "FROM PRODUCT_PRICE P INNER JOIN DEVALUE_HELP D " +
                //        "    ON P.PRODUCT_NO = D.PRODUCT_NO AND P.PACKING_TYPE = D.PACKING_TYPE " +
                //        "WHERE (D.CUST_NO = #CUST_NO OR D.CUST_NO = '*') " +
                //        "    AND F_DATE <= #GET_DATE AND T_DATE >= #GET_DATE "),
                //        new
                //        {
                //            CUST_NO = CustomerNo,
                //            GET_DATE = DbUtils._ToDbDate(DateTime.Now),
                //        }
                //    );

                //    return Json(new ApiResult
                //    {
                //        Data = data.Select(item => item.ToCamelCaseObject()),
                //    }, JsonRequestBehavior.AllowGet);
                //}
            }
            catch(Exception ex)
            {
                Log.Error(ex.Message, ex);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Upload unsuccessfully.");
            }
        }
    }
}
