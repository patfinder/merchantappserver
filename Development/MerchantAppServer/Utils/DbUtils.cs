﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SQLite;
using System.Text.RegularExpressions;
using System.Globalization;

namespace MerchantAppServer.Utils
{
    public static class DbUtils
    {
        public static IDbConnection Connection
        {
            get
            {
#if USESQLITE
                return new SQLiteConnection(ConnectionString);
#elif USEORACLE
                //var chars = "123456".ToCharArray();
                //var password = new System.Security.SecureString();
                //chars.ToList().ForEach(ch => password.AppendChar(ch));
                //password.MakeReadOnly();
                //return new OracleConnection(ConnectionString, new OracleCredential("mer_db", password));
                return new OracleConnection(ConnectionString);
#else
#endif
            }
        }

        public static string ConnectionString
        {
#if USESQLITE
            get => ConfigurationManager.ConnectionStrings["SqlLiteDb"].ConnectionString;
#elif USEORACLE
            get => ConfigurationManager.ConnectionStrings ["OracleDb"].ConnectionString;
#else
#endif
        }

        public const string DefaultOracleDateFormat = "yyyy-MM-dd";

        /// <summary>
        /// Time format string
        /// </summary>
        public const string TimeStampIdFormat = "yyMMddHHmmss";

        public const string ShortTimeIdStampFormat = "yyMMddHH";

        /// <summary>
        /// Replace params prefix with current DB corresponding chars.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static string _T(string sql)
        {
#if USESQLITE
            return sql.Replace("#", ":");
#elif USEORACLE
            return sql.Replace("#", ":");
#else
            // sql server
            //return sql.Replace("#", "@");
#endif
        }

        /// <summary>
        /// Replace SQL params. Note: case-insensitive
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static string _R(string sql, object paramss)
        {
            if (string.IsNullOrEmpty(sql) || paramss == null) return sql;

            sql = _T(sql);

            var type = paramss.GetType();
            var props = type.GetProperties();

            // Evaluator
            var eval = new MatchEvaluator((match) => {
                var name = match.Value.Substring(1);
                var prop = props.FirstOrDefault(p => p.Name.ToLower(CultureInfo.InvariantCulture) == name.ToLower(CultureInfo.InvariantCulture));
                if (prop == null) return match.Value;

                // String
                if (prop.PropertyType == typeof(string)) {
                    return $"'{prop.GetValue(paramss, null)}'";
                }

                // Date
                if (prop.PropertyType == typeof(DateTime))
                {
                    var val = ((DateTime)prop.GetValue(paramss, null)).ToString(DefaultOracleDateFormat, CultureInfo.InvariantCulture);
                    return $"TO_DATE('{val}', 'yyyy-mm-dd')";
                }

                // Enum
                if (prop.PropertyType.IsEnum)
                {
                    return $"{(int)prop.GetValue(paramss, null)}";
                }

                // Default, convert to string
                return $"'{prop.GetValue(paramss, null)}'";
            });

            var regex = new Regex(@":(\w+)");
            return regex.Replace(sql, eval);
        }

        /// <summary>
        /// Convert a C# name to DB name. Eg: CustomerNo -> CUSTOMER_NO
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string _ToDbName(string name)
        {
            var regex = new Regex("([A-Z])[a-z]+");

            return regex.Replace(name, (m) => "_" + m.Value.ToUpper()).Substring(1);
        }

        public static string _ToDbDate(DateTime date)
        {
#if USESQLITE
#elif USEORACLE
            return date.ToString("yyyy.MM.dd");
#else
            // sql server
#endif
        }

        public static int ExecuteNonQuery(string commandText, object[] parameters = null)
        {
            return ExecuteNonQuery(CommandType.Text, commandText, parameters ?? new object[0]);
        }

        public static int ExecuteNonQuery(CommandType commandType, string commandText, object[] parameters)
        {
            using (OracleConnection connection = new OracleConnection(ConnectionString))
            {
                connection.Open();

                using (OracleTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
                using (OracleCommand command = new OracleCommand(commandText, connection))
                {
                    command.Transaction = transaction;
                    command.CommandType = commandType;

                    AddParameters(command, parameters);

                    var resultCount = command.ExecuteNonQuery();
                    transaction.Commit();

                    return resultCount;
                }
            }
        }

        public static void AddParameters(OracleCommand command, object[] parameters)
        {
            if (command == null)
            {
                throw new ApplicationException("null Command");
            }

            if (parameters != null)
            {
                for (int i = 0; i < parameters.Length; i++)
                {
                    command.Parameters.Add(parameters[i] as OracleParameter);
                }
            }
        }

        public static object CreateParameter(string name, OracleDbType dataType, object parameterValue, int size = 0, ParameterDirection direction = ParameterDirection.Input)
        {
            OracleParameter parameter = new OracleParameter(name, dataType);

            parameter.Value = parameterValue;
            parameter.Direction = direction;

            if (size >= 0)
            {
                parameter.Size = size;
            }

            return parameter;
        }

        public static object CreateReturnParameter()
        {
            return new OracleParameter()
            {
                Direction = ParameterDirection.ReturnValue
            };
        }
    }
}
