﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SQLite;
using System.Globalization;
using System.Text.RegularExpressions;

namespace MerchantAppServer.Utils
{
    public static class StringUtils
    {
        public static bool IsAllCapitals(this string str)
        {
            if (string.IsNullOrEmpty(str)) return true;

            // Found a lower case char
            if (Regex.Match(str, @"[a-z]").Success) return false;

            return true;
        }

        /// <summary>
        /// Convert TitleCase string or CAPITALS_CASE string to camelCase string.
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public static string ToCamelCase(this string src)
        {
            if (string.IsNullOrEmpty(src)) return src;

            // ALL_CAPITALS chars
            if (src.IsAllCapitals())
            {
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                var parts = src.Split(new[] { '_' });

                return string.Join("", parts.Select((val, idx) => idx == 0 ? val.ToLower() : textInfo.ToTitleCase(val.ToLower())));
            }

            // Convert to lower only first char
            return src.Substring(0, 1).ToLower() + src.Substring(1);
        }
    }
}
