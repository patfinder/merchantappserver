﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SQLite;
using System.Dynamic;

namespace MerchantAppServer.Utils
{
    public static class LangUtils
    {
        public static object ToCamelCaseObject(this object obj)
        {
            dynamic obj2 = obj;
            var type = obj.GetType();

            // get public properties
            var props = type.GetProperties();

            //dynamic objOut = new MyDynamicObject();
            var objOut = new ExpandoObject();

            props.ToList().ForEach(prop => objOut.AddProperty(prop.Name.ToCamelCase(), prop.GetValue(obj)));

            return objOut;
        }

        public static void AddProperty(this ExpandoObject expando, string propertyName, object propertyValue)
        {
            // ExpandoObject supports IDictionary so we can extend it like this
            var expandoDict = expando as IDictionary<string, object>;
            if (expandoDict.ContainsKey(propertyName))
                expandoDict[propertyName] = propertyValue;
            else
                expandoDict.Add(propertyName, propertyValue);
        }
    }

    public class MyDynamicObject : DynamicObject
    {
        public class DynamicFormData : DynamicObject
        {
            private Dictionary<string, object> Fields = new Dictionary<string, object>();

            public int Count { get { return Fields.Keys.Count; } }

            public void Add(string name, string val = null)
            {
                if (!Fields.ContainsKey(name))
                {
                    Fields.Add(name, val);
                }
                else
                {
                    Fields[name] = val;
                }
            }

            public override bool TryGetMember(GetMemberBinder binder, out object result)
            {
                if (Fields.ContainsKey(binder.Name))
                {
                    result = Fields[binder.Name];
                    return true;
                }
                return base.TryGetMember(binder, out result);
            }

            public override bool TrySetMember(SetMemberBinder binder, object value)
            {
                if (!Fields.ContainsKey(binder.Name))
                {
                    Fields.Add(binder.Name, value);
                }
                else
                {
                    Fields[binder.Name] = value;
                }
                return true;
            }

            public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
            {
                if (Fields.ContainsKey(binder.Name) &&
                    Fields[binder.Name] is Delegate)
                {
                    Delegate del = Fields[binder.Name] as Delegate;
                    result = del.DynamicInvoke(args);
                    return true;
                }
                return base.TryInvokeMember(binder, args, out result);
            }
        }
    }
}
