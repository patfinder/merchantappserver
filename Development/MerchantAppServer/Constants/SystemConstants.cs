﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace MerchantAppServer.Constants
{
    public static class SystemConstants
    {
        public const string AuthenticationCookie = "auth-cookie";

        /// <summary>
        /// Default Page Size
        /// </summary>
        public const int DefaultPageSize = 20;

        /// <summary>
        /// 'Yes' flag value
        /// </summary>
        public const string YES_FLAG = "Y";
        
        /// <summary>
        /// 'No' flag value
        /// </summary>
        public const string NO_FLAG = "N";
    }
}
