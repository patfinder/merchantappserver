// PRODUCT_TABLE table columns
PRODUCT_NO
PACKING_TYPE
UNIT_PRICE
USE_DATE
USE_FLAG
PRODUCT_CNAME
PRODUCT_VNAME
PRODUCT_ENAME
PRODUCT_KIND


ProductNo
PackingType
UnitPrice
UseDate
UseFlag
ProductCname
ProductVname
ProductEname
ProductKind


// Product Price - PRODUCT_PRICE
PRODUCT_NO
PACKING_TYPE
UNIT_PRICE
USE_DATE
USE_FLAG

ProductNo
PackingType
UnitPrice
UseDate
UseFlag

// DEVALUE_HELP table columns

PRODUCT_NO
PACKING_TYPE
CUST_NO
F_DATE
T_DATE
PERCENT
DISCOUNT

ProductNo
PackingType
CustNo
FDate
TDate
Percent
Discount


// CUSTOMER

CUST_NO
CUST_VNAME
PASSWORD
LABEL_FLAG

CustNo
CustVname
Password
LabelFlag


// TRANS_COST_TABLE table columns

PLACE_NO
PLACE_NAME2
LIVESTOCK_VND
AQUA_VND
USE_FLAG
THE_DATE

PlaceNo
PlaceName2
LivestockVnd
AquaVnd
UseFlag
TheDate


// ORDER_MASTER
ORDER_NO
ORDER_DATE
PLATE_NO
PLACE_NO
CUST_NO
OK_FLAG
CREATE_DATE
CONFIRM_DATE
CONFIRM_USER
FINISH_DATE


OrderNo
OrderDate
PlateNo
PlaceNo
CustNo
OkFlag
CreateDate
ConfirmDate
ConfirmUser
FinishDate



// ORDER_DETAIL
ORDER_NO
S_NO
PRODUCT_NO
PACKING_TYPE
QTY
UNIT_PRICE

OrderNo
SNo
ProductNo
PackingType
Qty
UnitPrice



// Transport

TRANSPORT_NO
CUST_NO
PLATE_NO
TRANSPORT_NAME
DRIVER_NAME
MOBILE

TransportNo
CustNo
PlateNo
TransportName
DriverName
Mobile


