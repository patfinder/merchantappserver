
-- Common query parameters

-- var ViewDate VARCHAR2(10 BYTE)
-- exec :ViewDate := '2019.11.01'
-- var CustomerNo VARCHAR2(10 BYTE)
-- exec :CustomerNo := 'C30019'



-- ============================================================
-- Common DB check queries



-- ============================================================
-- Auth/Login - Login API

SELECT CUST_NO CustNo, CUST_VNAME CustVName, LABEL_FLAG LabelFlag FROM CUSTOMER
WHERE CUST_NO = #CustomerNo AND PASSWORD = #Password AND (IMEI IS NULL OR IMEI = #Imei)



select * from (select cust_no, imei, password,rownum from customer) where rownum < 10;

update customer set password='password1' where cust_no='C30019';


-- ============================================================
-- Get P1 List
SELECT B_CODE, B_VNAME FROM BIG_KIND ORDER BY B_VNAME;


-- ============================================================
-- Get products with discount (ProductService.GetCustomerProducts)

-- Nov 20 (sum discount)
-- TODO: R.USE_DATE, R.USE_FLAG

-- V4
SELECT P.P_1 P1, P.PRODUCT_NO ProductNo, P.PRODUCT_VNAME ProductVName, R.PACKING_TYPE PackingType, R.UNIT_PRICE UnitPrice,
    SUM(COALESCE(D.PERCENT, 0)) Percent, SUM(COALESCE(D.DISCOUNT, 0)) Discount
FROM PRODUCT_TABLE P 
    INNER JOIN PRODUCT_PRICE R ON (P.PRODUCT_NO = R.PRODUCT_NO AND R.USE_FLAG = '1')
    LEFT JOIN DEVALUE_HELP D ON R.PRODUCT_NO = D.PRODUCT_NO1 AND R.PACKING_TYPE = D.PACKING_TYPE
WHERE P.PRODUCT_KIND = 'P'
    AND (P.BRAND_NO = :LabelFlag OR P.BRAND_NO NOT IN ('1','2','3','5'))
    AND (
        D.PRODUCT_NO1 IS NULL 
        OR ((D.CUST_NO = '*' OR D.CUST_NO = :CustomerNo)
            AND :ViewDate BETWEEN F_DATE AND T_DATE)
    )
GROUP BY P.P_1, P.PRODUCT_NO, P.PRODUCT_VNAME, R.PACKING_TYPE, R.UNIT_PRICE
ORDER P.PRODUCT_VNAME

-- V3
SELECT P.PRODUCT_NO ProductNo, P.PRODUCT_VNAME ProductVName, R.PACKING_TYPE PackingType, R.UNIT_PRICE UnitPrice,
    SUM(COALESCE(D.PERCENT, 0)) Percent, SUM(COALESCE(D.DISCOUNT, 0)) Discount
FROM PRODUCT_TABLE P INNER JOIN PRODUCT_PRICE R ON (P.PRODUCT_NO = R.PRODUCT_NO AND R.USE_FLAG = '1')
    LEFT JOIN DEVALUE_HELP D ON R.PRODUCT_NO = D.PRODUCT_NO1 AND R.PACKING_TYPE = D.PACKING_TYPE
    LEFT JOIN CUSTOMER C ON (D.CUST_NO = C.CUST_NO)
WHERE P.PRODUCT_KIND = 'P'
    AND (D.CUST_NO = :CustomerNo OR D.CUST_NO = '*' OR D.CUST_NO IS NULL)
    AND :ViewDate BETWEEN F_DATE AND T_DATE
    AND (P.BRAND_NO = C.LABEL_FLAG OR P.BRAND_NO NOT IN ('1','2','3','5'))
GROUP BY P.PRODUCT_NO, P.PRODUCT_VNAME, R.PACKING_TYPE, R.UNIT_PRICE


-- V2 (remove R.USE_DATE, change F_DATE AND T_DATE cond, use COALESCE)
SELECT P.PRODUCT_NO ProductNo, P.PRODUCT_VNAME ProductVName, R.PACKING_TYPE PackingType, R.UNIT_PRICE UnitPrice,
    R.USE_FLAG UseFlag, COALESCE(D.PERCENT, 0) Percent, COALESCE(D.DISCOUNT, 0) Discount
FROM PRODUCT_TABLE P INNER JOIN PRODUCT_PRICE R ON P.PRODUCT_NO = R.PRODUCT_NO AND R.USE_FLAG = '1'
    LEFT JOIN (
        SELECT PRODUCT_NO, PACKING_TYPE, SUM(PERCENT), SUM(DISCOUNT)
        FROM DEVALUE_HELP
        WHERE (D.CUST_NO = #CustomerNo OR D.CUST_NO = '*')
            AND #ViewDate BETWEEN F_DATE AND T_DATE
        GROUP BY PRODUCT_NO, PACKING_TYPE
    ) D ON R.PRODUCT_NO = D.PRODUCT_NO AND R.PACKING_TYPE = D.PACKING_TYPE
WHERE (P.BRAND_NO = C.LABEL_FLAG OR P.BRAND_NO NOT IN ('1','2','3','5'))

-- V1
SELECT P.PRODUCT_NO ProductNo, P.PRODUCT_VNAME ProductVName, R.PACKING_TYPE PackingType, R.UNIT_PRICE UnitPrice,
    R.USE_DATE FDate, R.USE_FLAG UseFlag, D.PERCENT Percent, D.DISCOUNT Discount
FROM PRODUCT_TABLE P INNER JOIN PRODUCT_PRICE R ON P.PRODUCT_NO = R.PRODUCT_NO AND R.USE_FLAG = '1'
    LEFT JOIN (
        SELECT PRODUCT_NO, PACKING_TYPE, SUM(PERCENT), SUM(DISCOUNT)
        FROM DEVALUE_HELP
        WHERE D.CUST_NO = #CustomerNo OR D.CUST_NO = '*'
        GROUP BY PRODUCT_NO, PACKING_TYPE
    ) D ON R.PRODUCT_NO = D.PRODUCT_NO AND R.PACKING_TYPE = D.PACKING_TYPE
WHERE (P.BRAND_NO = C.LABEL_FLAG OR P.BRAND_NO NOT IN ('1','2','3','5'))
    AND #viewDate BETWEEN F_DATE AND T_DATE


-- Nov 18
SELECT P.PRODUCT_NO ProductNo, P.PRODUCT_VNAME ProductVName, R.PACKING_TYPE PackingType, R.UNIT_PRICE UnitPrice,
    R.USE_DATE FDate, R.USE_FLAG UseFlag, D.PERCENT Percent, D.DISCOUNT Discount
FROM PRODUCT_TABLE P INNER JOIN PRODUCT_PRICE R ON P.PRODUCT_NO = R.PRODUCT_NO AND R.USE_FLAG = '1'
    LEFT JOIN DEVALUE_HELP D ON P.PRODUCT_NO = D.PRODUCT_NO AND R.PACKING_TYPE = D.PACKING_TYPE
WHERE (D.CUST_NO = #CustomerNo OR D.CUST_NO = '*' OR D.CUST_NO IS NULL)
    AND #viewDate BETWEEN F_DATE AND T_DATE


-- Oct 30
SELECT P.PRODUCT_NO, P.PACKING_TYPE, P.UNIT_PRICE, P.USE_DATE, P.USE_FLAG,
    D.PERCENT, D.DISCOUNT
FROM PRODUCT_PRICE P INNER JOIN DEVALUE_HELP D
    ON P.PRODUCT_NO = D.PRODUCT_NO AND P.PACKING_TYPE = D.PACKING_TYPE
WHERE (D.CUST_NO = #CUST_NO OR D.CUST_NO = '*')
    AND F_DATE <= #GET_DATE AND T_DATE >= #GET_DATE


SELECT P.PRODUCT_NO, P.PACKING_TYPE, P.UNIT_PRICE, P.USE_DATE, P.USE_FLAG,
    D.PERCENT, D.DISCOUNT
FROM PRODUCT_PRICE P INNER JOIN DEVALUE_HELP D
    ON P.PRODUCT_NO = D.PRODUCT_NO AND P.PACKING_TYPE = D.PACKING_TYPE
WHERE (D.CUST_NO = '' OR D.CUST_NO = '*') AND F_DATE <= '2019.10.01' AND T_DATE >= '2019.09.30';


-- ============================================================
-- Get Order list

-- THIS NOT WORK --
SELECT * FROM (
    SELECT
        ord.*,
        row_number() OVER (ORDER BY ord.order_no ASC) line_number
    FROM order_master ord
) WHERE line_number BETWEEN 0 AND 5 ORDER BY line_number;


SELECT * FROM (
    SELECT
        ORD2.*,
        row_number() OVER (ORDER BY ORD2.ORDER_NO ASC) LINE_NUMBER
    FROM ORDER_MASTER ORD2)
WHERE LINE_NUMBER BETWEEN 
        ((#PAGE_NUMBER - 1) * #PAGE_SIZE + 1) AND
        ( #PAGE_NUMBER * #PAGE_SIZE))
ORDER BY LINE_NUMBER;

------------------

SELECT *
FROM (
    SELECT Q1.*, ROWNUM RN
    FROM (
        SELECT * FROM ORDER_MASTER WHERE CUST_NO = '#CUSTOMERNO'
        ORDER BY CREATE_DATE DESC
    ) Q1
    WHERE ROWNUM < #PAGE * #PAGESIZE + 1
)
WHERE RN >= ((#PAGE - 1) * #PAGESIZE) + 1

-- Get Previous Order list (Get orders in specified month)

SELECT {fieldsStr}
FROM ORDER_MASTER
WHERE CUST_NO = '#CUSTOMERNO'
	AND #DATE1 <= ORDER_DATE AND ORDER_DATE < #DATE2
ORDER BY CREATE_DATE DESC, ORDER_NO DESC 


-- V2

SELECT {fieldsStr} 
FROM ( 
    SELECT {innerFieldsStr}, ROWNUM RN
    FROM ( 
        SELECT {innerFieldsStr}
		FROM ORDER_MASTER
		WHERE CUST_NO = '#CUSTOMERNO'
			AND #DATE1 <= ORDER_DATE AND ORDER_DATE < #DATE2
        ORDER BY CREATE_DATE DESC, ORDER_NO DESC 
    ) Q1 
    WHERE ROWNUM < #PAGE * #PAGESIZE + 1 
) 
WHERE RN >= ((#PAGE - 1) * #PAGESIZE) + 1

-- Create Trasnport

INSERT INTO TRANSPORT_TABLE(TRANSPORT_NO, CUST_NO, PLATE_NO, TRANSPORT_NAME, DRIVER_NAME, MOBILE)
VALUES(#TRANSPORT_NO, #CUST_NO, #PLATE_NO, #TRANSPORT_NAME, #DRIVER_NAME, #MOBILE)

-- Create Order


INSERT INTO ORDER_MASTER(ORDER_NO, ORDER_DATE, PLATE_NO, PLACE_NO, CUST_NO, OK_FLAG, CREATE_DATE)
VALUES(#ORDER_NO, #ORDER_DATE, #PLATE_NO, #PLACE_NO, #CUST_NO, #OK_FLAG, #CREATE_DATE)

-- Create Order Detail

INSERT INTO ORDER_DETAIL(ORDER_NO, S_NO, PRODUCT_NO, PACKING_TYPE, QTY, UNIT_PRICE)
VALUES (#ORDER_NO, #S_NO, #PRODUCT_NO, #PACKING_TYPE, #QTY, #UNIT_PRICE)

-- Get Order Count of last month

SELECT * FROM ORDER_MASTER
WHERE ORDER_DATE = 


-- map complex nested object
-- https://github.com/SlapperAutoMapper/Slapper.AutoMapper


SELECT 
    M.ORDER_NO AS ORDER_NO,
    M.ORDER_DATE AS ORDER_MASTER_ORDER_DATE,
    D.PRODUCT_NO AS ORDER_DETAIL_PRODUCT_NO
FROM ORDER_MASTER M
INNER JOIN ORDER_DETAIL D ON M.ORDER_NO = D.ORDER_NO
WHERE ROWNUM = 1;




